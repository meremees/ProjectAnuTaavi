package ee.gallery.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import ee.gallery.data.GalleryByCategorie;

public class GalleryByCategorieResource {
	public Set<GalleryByCategorie> getAllGalleryByCategorie() {
		Set<GalleryByCategorie> categories = new HashSet<>();
		String sqlQuery = "SELECT * FROM gallery_data";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				GalleryByCategorie categorie = new GalleryByCategorie()
						.setidGallery(results.getInt("idGallery"))
						.setjeweleryName(results.getString("jeweleryName"))
						.setcategorieName(results.getString("categorieName")).setmaterial(results.getString("material"))
						.setexplanation(results.getString("explanation"));
				categories.add(categorie);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting data set: " + e.getMessage());
		}
		return categories;
	}
}

package ee.gallery.resource;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {
	private static Connection connection = null;

	public static Connection getConnection() {
		String dbUrl = "jdbc:mysql://localhost:3306/galerii_andmed";
		Properties connectionProperties = new Properties();
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "ValiItSalas6na");

		try {
			connection = DriverManager.getConnection(dbUrl, connectionProperties);
		} catch (SQLException e) {
			System.out.println("Error on creating database connection: " + e.getMessage());
		}
		return connection;
	}

	public static void loadDriver() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			System.out.println("Error on loadingdriver: " + e.getMessage());
		}

	}

	// andmebaasi ühendus kinni
	public static void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("Error on closing database connection: " + e.getMessage());

		}
	}

}

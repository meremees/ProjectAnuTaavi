package ee.gallery.data;

public class GalleryByCategorie {
	private int idGallery;
	private String jeweleryName;
	private String categorieName;
	private String material;
	private String explanation;

	public int getidGallery() {
		return idGallery;
	}

	public GalleryByCategorie setidGallery(int idGallery) {
		this.idGallery = idGallery;
		return this;
	}

	public String getjeweleryName() {
		return jeweleryName;
	}

	public GalleryByCategorie setjeweleryName(String jeweleryName) {
		this.jeweleryName = jeweleryName;
		return this;
	}

	public String getcategorieName() {
		return categorieName;
	}

	public GalleryByCategorie setcategorieName(String categorieName) {
		this.categorieName = categorieName;
		return this;
	}

	public String getmaterial() {
		return material;
	}

	public GalleryByCategorie setmaterial(String material) {
		this.material = material;
		return this;
	}

	public String getexplanation() {
		return explanation;
	}

	public GalleryByCategorie setexplanation(String explanation) {
		this.explanation = explanation;
		return this;
	}

	@Override
	public String toString() {
		return "GalleryByCategorie[idGallery=" + idGallery + ", jeweleryName=" + jeweleryName
				+ ", categorieName= " + categorieName + ", material=" + material + ", explanation= " + explanation + "]";
	}

}

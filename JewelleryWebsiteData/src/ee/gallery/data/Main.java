package ee.gallery.data;

import java.util.Set;

import ee.gallery.resource.GalleryByCategorieResource;

public class Main {
	public static void main(String[] args) {
		Set<GalleryByCategorie> categories;
		GalleryByCategorieResource resource = new GalleryByCategorieResource();
		categories = resource.getAllGalleryByCategorie();
		System.out.println(categories);

	}

}

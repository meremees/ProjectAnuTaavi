INSERT INTO gallery_data (jeweleryName, categorieName, material, explanation) VALUES ('Maskeraad', 'skulptuur', 'pronks, hõbe 925', 'Filmiauhindade gala 2014, elutöö preemia laureaadile. Ainueksemplar.'); 
INSERT INTO gallery_data  (jeweleryName, categorieName, material, explanation) VALUES ('Võti', 'skulptuur', 'pronks, hõbe 925', 'Filmiauhindade gala 2014, uue tulija preemia. Ainueksemplar.');
INSERT INTO gallery_data  (jeweleryName, categorieName, material, explanation) VALUES ('Puuslik', 'kaelakee', 'puit, tekstiil, pronks', 'Näituseksponaat “Sügisproloog”, A-Galerii, 2016');
INSERT INTO gallery_data  (jeweleryName, categorieName, material, explanation) VALUES ('Moon disc', 'käevõru', 'hõbe 925', 'Valmistatud eritellimusena, ainueksemplar.');
INSERT INTO gallery_data  (jeweleryName, categorieName, material, explanation) VALUES ('Südamega', 'pross', 'pronks, puit, merevaik', 'Valmistatud eritellimusena, ainueksemplar.');
INSERT INTO gallery_data  (jeweleryName, categorieName, material, explanation) VALUES ('Kivivõru', 'kaelakee', 'hõbe 925', 'Valmistatud eritellimusena, ainueksemplar.');

SELECT * FROM  gallery_data  WHERE categorieName = 'pross';
SELECT * FROM  gallery_data  WHERE material = 'hõbe 925';
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: galerii_andmed
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ehtedandmebaasis`
--

DROP TABLE IF EXISTS `ehtedandmebaasis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ehtedandmebaasis` (
  `idehtedandmebaasis` int(11) NOT NULL,
  `ehte_nimi` varchar(20) DEFAULT NULL,
  `kategooria` varchar(15) DEFAULT NULL,
  `materjal` varchar(45) DEFAULT NULL,
  `hind` varchar(15) DEFAULT NULL,
  `ooteaeg` varchar(15) DEFAULT NULL,
  `mõõdud` varchar(100) DEFAULT NULL,
  `haiku` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idehtedandmebaasis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehtedandmebaasis`
--

LOCK TABLES `ehtedandmebaasis` WRITE;
/*!40000 ALTER TABLE `ehtedandmebaasis` DISABLE KEYS */;
INSERT INTO `ehtedandmebaasis` VALUES (1,'Pisarad','kõrvarõngad','hõbe 925, kristall','117 ','3 päeva','kaal 4,3g','Ootamist täis aeg.'),(2,'Rings','sõrmus','hõbe 925','85','5 päeva','kaal 4,7g, läbimõõt 1,7 cm','Kaks silmapaari, õhkõrnalt ootust lootes, ootama peavad'),(3,'Päikeseketas','käevõru','kullatud, hõbe 925','270','5 päeva','kaal 10 g kõrgus 1,3 cm diameeter 16,5 cm','Kullaketrajad, hing palmikusse seotud, kiir läbimas kiirt.'),(4,'Mustikas','kõrvarõngad','hõbe 925, must pärl','65','3 päeva','kaal 4,3g kõrgus 5 cm laius 0,8 cm','Maha jätmata tardunud poeesias koduteed otsib'),(5,'Mustikas','käevõru','hõbe 925, must pärl','749','5 päeva','kaal 45 g kõrgus 4,5 cm diameeter 16,5 cm','Sirguv marjalaps metsvõrsete saginas veetlust viimistleb '),(6,'Mustikas','käevõru','hõbe 925, must pärl','115','4 päeva','kaal 8,8 g kõrgus 7,5 cm laius 5,4','Karges lummuses sosistav metsahein mõtleb kevadest'),(7,'Ketli','pross','hõbe, tekstiil, värv','110','3 päeva','kaal 12,6g   kõrgus 7,8 cm  laius 9 cm ','Pilku pilgates naerusuu hoogu võtab, meetreid ei jagu.'),(8,'Tilk','kõrvarõngad','hõbe 925, punane korall','50','3 päeva','kaal 2,6g   kõrgus 4,8 cm  laius 1 cm ','Kirepunane auväljal ristatuna võitluses võitja'),(9,'Kristi','pross','hõbe, lehtkuld','370','4 päeva','kaal 12,6g   kõrgus 7,8 cm  laius 9 cm ','Käest kinni hoides, samm sammult seista lastes, kõnnib omapäi.'),(10,'Roos','pross','pronks','120','8 päeva','kaal 34,6g   kõrgus 13,8 cm  laius 9 cm ','Nõtkuse hetkel kalambuuri visates sõnaga nõus'),(11,'Vihm','kõrvarõngad','hõbe 925','69','3 päeva','kaal 2,6g   kõrgus 1,8 cm  laius 1 cm ','Kaks härmapiiska napisõnalistena vaka all peidus'),(12,'Märkmed','mansetinööbid','hõbe 925','91','6 päeva','kaal 5,6g   kõrgus 1,2 cm  laius 1 cm ','Silma vaadates sõna kasvamist näeb. Vägi teed leiab.');
/*!40000 ALTER TABLE `ehtedandmebaasis` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-09 11:32:18

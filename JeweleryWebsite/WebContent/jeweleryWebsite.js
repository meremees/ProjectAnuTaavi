var allJewelleries;
function getAllJewelleries() {
	$.getJSON("http://localhost:8080/JeweleryWebsite/rest/jewelleries",
			function(jewelleries) {
				allJewelleries = jewelleries;
				console.log(jewelleries);
				var div = document.getElementById("jewelery1");
				var divContent = "<table>";
				for (var i = 0; i < jewelleries.length; i = i + 3) {
					divContent = divContent + "<tr><td>"
							+ createMiniTable(jewelleries[i]) + "</td><td>"
							+ createMiniTable(jewelleries[i + 1]) + "</td><td>"
							+ createMiniTable(jewelleries[i + 2])
							+ "</td></tr>";

				}
				divContent = divContent + "</table>";
				div.innerHTML = div.innerHTML + divContent;

			});
}
getAllJewelleries();

function createMiniTable(singleItem) {
	return "<table id='mainJewelryTable'>"
			+ "<tr><td rowspan='5'><img src='img/"
			+ (singleItem.imgName)
			+ "' width='150' height='150' onMouseOver=\"showHaiku('haiku"
			+ singleItem.idJewelery
			+ "')\"></td><td><b>"
			+ (singleItem.jeweleryName)
			+ "</b></td></tr>"
			+ "<tr><td>"
			+ (singleItem.category)
			+ "</td></tr>"
			+ "<tr><td>"
			+ (singleItem.material)
			+ "</td></tr>"
			+ "<tr><td>"
			+ (singleItem.price)
			+ " €"
			+ "</td></tr>"
			+ "<tr><td>"
			+ "<div id='dimension"
			+ singleItem.idJewelery
			+ "'>"
			+ "<button type='button' class='btn btn-light btn-xs active'>"
			+ "<i class='material-icons' style='font-size:20px;color:#303a40' title='Mõõdud, kaal' data-toggle='popover' data-placement='bottom' data-trigger='hover' data-content='"
			+ singleItem.dimension
			+ "'>signal_cellular_null</i></button>"
			+ "<button type='button' class='btn btn-light btn-xs active'>"
			+ "<i class='material-icons' style='font-size:20px;color:#303a40' title='Ooteaeg' data-toggle='popover' data-placement='bottom' data-trigger='hover' data-content='"
			+ singleItem.waitingTime
			+ "'>access_time</i></button>"
			+ "<button type='button' class='btn btn-light btn-xs active' onClick='fillEmailModal("
			+ singleItem.idJewelery
			+ ")' data-toggle='modal' data-target='#messageModal'data-whatever='@mdo' >"
			+ "<i class='material-icons' style='font-size:20px;color:#303a40' title='Any question about this Item?' data-toggle='popover' data-placement='bottom' data-trigger='hover' data-content='Onclik!'>mail_outline</i></button>"
			+ "<button type='button' class='btn btn-light btn-xs active' onClick='addItemToCart("
			+ singleItem.idJewelery
			+ ")'><i class='material-icons' style='font-size:20px;color:#303a40' title='Toote tellimine' data-toggle='popover' data-placement='bottom' data-trigger='hover' data-content='Onclick will add to shoping cart'>shop</i></button></td>"
			+ "</div>"
			+ "</td></tr>"
			+ "<div id='haiku"
			+ singleItem.idJewelery
			+ "' style=\"display: none \"><i>"
			+ (singleItem.haiku) + "</i></div>" + "</table>";
}

var allCategories;
function getAllCategories() {
	$.getJSON("http://localhost:8080/JeweleryWebsite/rest/jewelleries",
			function(jewelleries) {
				allJewelleries = jewelleries;
				console.log(jewelleries);
				var div = document.getElementById("jewelery1");
				var divContent = "<table id='mainJewelryTable'>";
				for (var i = 0; i < jewelleries.length; i = i + 3) {
					divContent = divContent + "<tr><td>"
							+ createMiniTable(jewelleries[i]) + "</td><td>"
							+ createMiniTable(jewelleries[i + 1]) + "</td><td>"
							+ createMiniTable(jewelleries[i + 2])
							+ "</td></tr>";

				}
				divContent = divContent + "</table>";
				div.innerHTML = divContent;

			});
}

function fillCartModal() {
	var cartTableBody = document.getElementById("cartTableBody");
	var cartTableBodyContent = "";
	var cartTotal = 0;
	for (var i = 0; i < arrayWithClickedItem.length; i++) {
		cartTableBodyContent = cartTableBodyContent
				+ "<tr><td>"
				+ arrayWithClickedItem[i].jeweleryName
				+ "</td><td> "
				+ arrayWithClickedItem[i].price
				+ " €"
				+ "</td><td>"
				+ "<button type='button' class='close' onClick='deleteItemFromCart("
				+ i + ")'>&times;</button>" + "</td></tr>";
		cartTotal = cartTotal + arrayWithClickedItem[i].price;

	}
	cartTableBody.innerHTML = cartTableBodyContent;
	document.getElementById("cartTotal").innerHTML = cartTotal + " €";
}

function deleteItemFromCart(index) {
	arrayWithClickedItem.splice(index, 1);
	console.log(arrayWithClickedItem);
	fillCartModal();
}

var arrayWithClickedItem = new Array();
function addItemToCart(shopingItemId) {
	for (var i = 0; i < allJewelleries.length; ++i) {
		if (allJewelleries[i].idJewelery == shopingItemId) {
			arrayWithClickedItem.push(allJewelleries[i]);
			console.log(arrayWithClickedItem);
		}
	}
}

function showHaiku(haikuId) { // haiku ID on määratud //tegelikult pole enam
	// vaja, kuid pedagoogika mõttes las olla
	// console.log("tahan peita haikut id-ga: " + haikuId)
	showHideElementById(haikuId);

}

function showHideElementById(elementId) {
	var x = document.getElementById(elementId);
	if (x.style.display === "none") {
		x.style.display = "block";
	} else {
		x.style.display = "none";
	}
}

function getJewelerySelection() {
	var categoryIdSelect = document
			.getElementById("getElementByCategoryIdSelect");
	var selectedCategoryId = categoryIdSelect.options[categoryIdSelect.selectedIndex].value;
	getJewelleriesByCategoryId(selectedCategoryId);

}

function getJewelleriesByCategoryId(selectedCategoryId) {
	if (selectedCategoryId == 0) {
		document.getElementById("jewelery1").innerHTML = "";
		getAllJewelleries();
	} else {
		$
				.getJSON(
						"http://localhost:8080/JeweleryWebsite/rest/jewelleries/categories/"
								+ selectedCategoryId,
						function(jewelleries) {
							console.log(jewelleries);
							var div = document.getElementById("jewelery1");
							var divContent = ""; // tõstsime VARi siia, et
							// saaksime sama
							// DIVi kasutada
							if (jewelleries.length == 0) {
								divContent = "Hetkel pole valikus.";
							} else {
								divContent = "<table>";
								for (var i = 0; i < jewelleries.length; i = i + 3) {
									// peame tegema tingimuslasuse juhtumiteks,
									// kui
									// eset
									// pole üldse või kui pole 3 jaguv
									// kui pikkus % 3 == 1, siis viimasesse
									// ritta
									// jääb 1
									// kui pikkus % 3 == 2, siis viimasesse
									// ritta
									// jääb 2
									// kui pikkus % 3 == 3, siis viimasesse
									// ritta
									// jääb 3
									if ((jewelleries.length % 3 == 0 || (jewelleries.length - 1)
											- (i + 2)) > 0) {
										divContent = divContent
												+ "<tr><td>"
												+ createMiniTable(jewelleries[i])
												+ "</td><td>"
												+ createMiniTable(jewelleries[i + 1])
												+ "</td><td>"
												+ createMiniTable(jewelleries[i + 2])
												+ "</td></tr>";
									} else if ((jewelleries.length % 3 == 2 && (jewelleries.length - 1)
											- (i + 2)) < 0) {
										divContent = divContent
												+ "<tr><td>"
												+ createMiniTable(jewelleries[i])
												+ "</td><td>"
												+ createMiniTable(jewelleries[i + 1])
												+ "</td><td></td></tr>";
									} else {
										divContent = divContent
												+ "<tr><td>"
												+ createMiniTable(jewelleries[i])
												+ "</td><td></td><td></td></tr>";
									}
								}
								divContent = divContent + "</table>";
							}
							div.innerHTML = divContent;

						});
	}
}
// GALERII OSA ALGUS

var allGalleries;
function getAllGalleries() {
	$.getJSON("http://localhost:8080/JeweleryWebsite/rest/galleries", function(
			galleries) {
		allGalleries = galleries;
		console.log(galleries);
		var div = document.getElementById("gallery1");
		var divContent = "<table>";
		for (var i = 0; i < galleries.length; i = i + 3) {
			divContent = divContent + "<tr><td>"
					+ createMiniGalleryTable(galleries[i]) + "</td><td>"
					+ createMiniGalleryTable(galleries[i + 1]) + "</td><td>"
					+ createMiniGalleryTable(galleries[i + 2]) + "</td></tr>";
		}
		divContent = divContent + "</table>";
		div.innerHTML = div.innerHTML + divContent;
	});
}
getAllGalleries();

function createMiniGalleryTable(singleItem) {
	return "<table id='mainGalleryTable'>"
			+ "<tr><td rowspan='5'><img src='img/"
			+ (singleItem.imgNameGallery)
			+ "' width='150' height='150' onMouseOver=\"showExplanation('explanationGallery"
			+ singleItem.idGallery
			+ "')\">"
			+ "</td><td><b>"
			+ (singleItem.jeweleryNameGallery)
			+ "</b></td></tr>"
			+ "<tr><td>"
			+ (singleItem.categoryGallery)
			+ "</td></tr>"
			+ "<tr><td>"
			+ (singleItem.materialGallery)
			+ "</td></tr>"
			+ "<div id='explanationGallery"
			+ singleItem.idGallery
			+ "' style=\"display: none \"><i>"
			+ (singleItem.explanationGallery) + "</i></div>" + "</table>";

}
function showExplanation(explanationGalleryId) {
	console.log("tahan peita explanationit id-ga: " + explanationGalleryId)
	showHideElementById(explanationGalleryId);

}
function getGallerySelection() {
	var categoryGalleryIdSelect = document
			.getElementById("getElementByGalleryCategoryIdSelect");
	var selectedCategoryGalleryId = categoryGalleryIdSelect.options[categoryGalleryIdSelect.selectedIndex].value;
	getGalleriesCategoryGalleryId(selectedCategoryGalleryId);

}
function getGalleriesCategoryGalleryId(selectedCategoryGalleryId) {
	if (selectedCategoryGalleryId == 0) {
		document.getElementById("gallery1").innerHTML = "";
		getAllGalleries();
	} else {
		$
				.getJSON(
						"http://localhost:8080/JeweleryWebsite/rest/galleries/categories/"
								+ selectedCategoryGalleryId,
						function(galleries) {
							console.log(galleries);
							var div = document.getElementById("gallery1");
							var divContent = ""; // tõstsime VARi siia, et
							// saaksime sama
							// DIVi kasutada
							if (galleries.length == 0) {
								divContent = "Pole galeriis.";
							} else {
								divContent = "<table>";
								for (var i = 0; i < galleries.length; i = i + 3) {
									// peame tegema tingimuslasuse juhtumiteks,
									// kui
									// eset
									// pole üldse või kui pole 3 jaguv
									// kui pikkus % 3 == 1, siis viimasesse
									// ritta
									// jääb 1
									// kui pikkus % 3 == 2, siis viimasesse
									// ritta
									// jääb 2
									// kui pikkus % 3 == 3, siis viimasesse
									// ritta
									// jääb 3
									if ((galleries.length % 3 == 0 || (galleries.length - 1)
											- (i + 2)) > 0) {
										divContent = divContent
												+ "<tr><td>"
												+ createMiniGalleryTable(galleries[i])
												+ "</td><td>"
												+ createMiniGalleryTable(galleries[i + 1])
												+ "</td><td>"
												+ createMiniGalleryTable(galleries[i + 2])
												+ "</td></tr>";
									} else if ((galleries.length % 3 == 2 && (galleries.length - 1)
											- (i + 2)) < 0) {
										divContent = divContent
												+ "<tr><td>"
												+ createMiniGalleryTable(galleries[i])
												+ "</td><td>"
												+ createMiniGalleryTable(galleries[i + 1])
												+ "</td><td></td></tr>";
									} else {
										divContent = divContent
												+ "<tr><td>"
												+ createMiniGalleryTable(galleries[i])
												+ "</td><td></td><td></td></tr>";
									}
								}
								divContent = divContent + "</table>";
							}
							div.innerHTML = divContent;

						});
	}
}
// GALERII OSA LÕPP

var selectedJeweleryId;
function fillEmailModal(selectedJeweleryIdFromItem) {
	selectedJeweleryId = selectedJeweleryIdFromItem;
}
function sendEmail() {
	var newEmailJson = {
		"idJewelery" : selectedJeweleryId,
		"senderEmail" : document.getElementById("contact-email").value,
		"message" : document.getElementById("contact-message").value

	}
	console.log(newEmailJson);
	var newEmail = JSON.stringify(newEmailJson);

	$.ajax({
		url : "http://localhost:8080/JeweleryWebsite/rest/emails",
		type : "POST",
		data : newEmail,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$('#messageModal').modal('hide');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);

		}
	});

}
$('#messageModal').on('show.bs.modal', function(event) {
	var button = $(event.relatedTarget)
	var recipient = button.data('whatever')
	var modal = $(this)
	modal.find('.modal-title').text('New message to ' + recipient)
	modal.find('.modal-body input').val(recipient)
})

$(document).ready(function() {
	$('[data-toggle="popover"]').popover();
});
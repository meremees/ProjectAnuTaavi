package ee.bcs.koolitus.jeweleryWebsite.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.jeweleryWebsite.dao.Gallery;



public class GalleryResource {
	

		public static List<Gallery> getAllGalleries() {
			List<Gallery> galleries = new ArrayList<>();
			String sqlQuery = "SELECT * FROM galerii";
			try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
				while (results.next()) {
					Gallery gallery = new Gallery().setIdGallery(results.getInt("idGallery")).setJeweleryNameGallery(results.getString("jeweleryNameGallery"))
							.setCategoryGallery(results.getString("categoryGallery")).setCategoryGalleryId(results.getInt("categoryGalleryId"))
							.setMaterialGallery(results.getString("materialGallery")).setExplanationGallery(results.getString("explanationGallery"))
							.setImgNameGallery(results.getString("imgNameGallery"));
					galleries.add(gallery);

				}
				return galleries;

			} catch (SQLException e) {
				System.out.println("Error getting address set: " + e.getMessage());
			}
			return galleries;
		}
		public static List<Gallery> getGalleriescategoryGalleryId(int categoryGalleryId) {
			List<Gallery> galleries = new ArrayList<>();
			String sqlQuery = "SELECT * FROM galerii WHERE categoryGalleryId=" + categoryGalleryId;
			try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
				while (results.next()) {
					Gallery gallery = new Gallery().setIdGallery(results.getInt("idGallery")).setJeweleryNameGallery(results.getString("jeweleryNameGallery"))
							.setCategoryGallery(results.getString("categoryGallery")).setCategoryGalleryId(results.getInt("categoryGalleryId"))
							.setMaterialGallery(results.getString("materialGallery")).setExplanationGallery(results.getString("explanationGallery"))
							.setImgNameGallery(results.getString("imgNameGallery"));
					galleries.add(gallery);

				}
				return galleries;

			} catch (SQLException e) {
				System.out.println("Error getting address set: " + e.getMessage());
			}
			return galleries;
	}
}

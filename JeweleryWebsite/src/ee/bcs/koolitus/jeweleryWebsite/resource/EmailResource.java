package ee.bcs.koolitus.jeweleryWebsite.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.jeweleryWebsite.dao.Email;


public class EmailResource {
	public static List<Email> getAllEmails() {
		List<Email> emails = new ArrayList<>();
		String sqlQuery = "SELECT * FROM email";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Email email = new Email().setIdJewelery(results.getInt("idJewelery"))
						.setSenderEmail(results.getString("senderEmail")).setMessage(results.getString("message")).setIdEmail(results.getInt("idEmail"));
				emails.add(email);

			}
			return emails;

		} catch (SQLException e) {
			System.out.println("Error getting address set: " + e.getMessage());
		}
		return emails;
	}

	public static Email fillEmailModal(Email email) {
		System.out.println(email);
		String sqlQuery = "INSERT INTO email (idJewelery, senderEmail, message) VALUES ("+ (email.getIdJewelery()==0 ? null:email.getIdJewelery()) + ", '"+ email.getSenderEmail() + "','"
				+ email.getMessage() + "')";
		System.out.println(sqlQuery);
		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				email.setIdEmail(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding address: " + e.getMessage());
		}
		return email;
	}
}

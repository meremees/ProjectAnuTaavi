package ee.bcs.koolitus.jeweleryWebsite.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import ee.bcs.koolitus.jeweleryWebsite.dao.Jewelery;

public class JeweleryResource {

	public static List<Jewelery> getAllJewelleries() {
		List<Jewelery> jewelleries = new ArrayList<>();
		String sqlQuery = "SELECT * FROM jeweleryList";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Jewelery jewelery = new Jewelery().setIdJewelery(results.getInt("idJewelery"))
						.setJeweleryName(results.getString("jeweleryName")).setCategoryId(results.getInt("categoryId"))
						.setMaterial(results.getString("material")).setPrice(results.getInt("price"))
						.setWaitingTime(results.getString("waitingTime")).setDimension(results.getString("dimension"))
						.setHaiku(results.getString("haiku")).setCategory(results.getString("category")).setImgName(results.getString("imgName"));
				jewelleries.add(jewelery);

			}
			return jewelleries;

		} catch (SQLException e) {
			System.out.println("Error getting address set: " + e.getMessage());
		}
		return jewelleries;
	}

	public static List<Jewelery> getJewelleriesByCategoryId(int categoryId) {
		List<Jewelery> jewelleries = new ArrayList<>();
		String sqlQuery = "SELECT * FROM jeweleryList WHERE categoryId=" + categoryId;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Jewelery jewelery = new Jewelery().setIdJewelery(results.getInt("idJewelery"))
						.setJeweleryName(results.getString("jeweleryName")).setCategoryId(results.getInt("categoryId"))
						.setMaterial(results.getString("material")).setPrice(results.getInt("price"))
						.setWaitingTime(results.getString("waitingTime")).setDimension(results.getString("dimension"))
						.setHaiku(results.getString("haiku")).setCategory(results.getString("category")).setImgName(results.getString("imgName"));
				jewelleries.add(jewelery);

			}
			return jewelleries;

		} catch (SQLException e) {
			System.out.println("Error getting address set: " + e.getMessage());
		}
		return jewelleries;
}

	public static void upDateJewelery(Jewelery jewelery) {
		// TODO Auto-generated method stub
		
	}
	
}



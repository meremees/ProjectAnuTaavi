package ee.bcs.koolitus.jeweleryWebsite.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.jeweleryWebsite.dao.Gallery;
import ee.bcs.koolitus.jeweleryWebsite.resource.GalleryResource;

@Path("/galleries")
public class GalleryController {

	@GET
		@Produces(MediaType.APPLICATION_JSON)
		public List<Gallery> getAllGalleries() {
			return GalleryResource.getAllGalleries();
	}	
	@GET
	@Path("/categories/{categoryGalleryId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Gallery> getGalleriescategoryGalleryId(@PathParam("categoryGalleryId") int categoryGalleryId) {
	return GalleryResource.getGalleriescategoryGalleryId(categoryGalleryId);
			}
		}


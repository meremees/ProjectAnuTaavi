package ee.bcs.koolitus.jeweleryWebsite.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import ee.bcs.koolitus.jeweleryWebsite.dao.Email;
import ee.bcs.koolitus.jeweleryWebsite.resource.EmailResource;

@Path("/emails")
public class EmailsController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Email> getAllEmails() {
		return EmailResource.getAllEmails();
	}
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Email addNewEmail(Email email) {
		return EmailResource.fillEmailModal(email);
	
	}
}

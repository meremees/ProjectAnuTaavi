package ee.bcs.koolitus.jeweleryWebsite.controller;

import java.util.List;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.bcs.koolitus.jeweleryWebsite.dao.Jewelery;
import ee.bcs.koolitus.jeweleryWebsite.resource.JeweleryResource;

@Path("/jewelleries")
public class JeweleryController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Jewelery> getAllJewelleries() {
		return JeweleryResource.getAllJewelleries();
	}

	@GET
	@Path("/categories/{categoryId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Jewelery> getJewelleriesByCategoryId(@PathParam("categoryId") int categoryId) {
		return JeweleryResource.getJewelleriesByCategoryId(categoryId);
	}

	
}

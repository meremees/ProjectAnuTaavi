package ee.bcs.koolitus.jeweleryWebsite.dao;

public class Jewelery {
	private int idJewelery;
	private String jeweleryName;
	private int categoryId;
	private String material;
	private int price;
	private String waitingTime;
	private String dimension;
	private String haiku;
	private String category;
	private String imgName;
	
	
	
	
	public String getImgName() {
		return imgName;
	}

	public Jewelery setImgName(String imgName) {
		this.imgName = imgName;
		return this;
	}

	public String getCategory() {
		return category;
	}
	
	public Jewelery setCategory(String category ) {
		this.category = category;
		return this;
		
	}

	public int getIdJewelery() {
		return idJewelery;
	}

	public Jewelery setIdJewelery(int idJewelery) {
		this.idJewelery = idJewelery;
		return this;
	}

	public String getJeweleryName() {
		return jeweleryName;
	}

	public Jewelery setJeweleryName(String jeweleryName) {
		this.jeweleryName = jeweleryName;
		return this;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public Jewelery setCategoryId(int categoryId) {
		this.categoryId = categoryId;
		return this;
	}

	public String getMaterial() {
		return material;
	}

	public Jewelery setMaterial(String material) {
		this.material = material;
		return this;
	}

	public int getPrice() {
		return price;
	}

	public Jewelery setPrice(int price) {
		this.price = price;
		return this;
	}

	public String getWaitingTime() {
		return waitingTime;
	}

	public Jewelery setWaitingTime(String waitingTime) {
		this.waitingTime = waitingTime;
		return this;
	}

	public String getDimension() {
		return dimension;
	}

	public Jewelery setDimension(String dimension) {
		this.dimension = dimension;
		return this;
	}

	public String getHaiku() {
		return haiku;
	}

	public Jewelery setHaiku(String haiku) {
		this.haiku = haiku;
		return this;
	}
	
	@Override
	public String toString() {
		return "Jewelery[idJewelery=" + idJewelery + " , material=" + material + " waitingTime=" + waitingTime
				+ " , jeweleryName=" + jeweleryName + " , categoryId=" + categoryId + " , price=" + price
				+ " , dimension=" + dimension + " , haiku=" + haiku + "]";

	}

	
}

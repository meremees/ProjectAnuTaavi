package ee.bcs.koolitus.jeweleryWebsite.dao;

public class Gallery {
	
private int idGallery;
private String jeweleryNameGallery;
private String categoryGallery;
private String materialGallery;
private String explanationGallery;
private int categoryGalleryId;
private String imgNameGallery;

public int getIdGallery() {
	return idGallery;
}
public Gallery setIdGallery(int idGallery) {
	this.idGallery = idGallery;
	return this;
}
public String getJeweleryNameGallery() {
	return jeweleryNameGallery;
}
public Gallery setJeweleryNameGallery(String jeweleryNameGallery) {
	this.jeweleryNameGallery = jeweleryNameGallery;
	return this;
}
public String getCategoryGallery() {
	return categoryGallery;
}
public Gallery setCategoryGallery(String categoryGallery) {
	this.categoryGallery = categoryGallery;
	return this;
}
public String getMaterialGallery() {
	return materialGallery;
}
public Gallery setMaterialGallery(String materialGallery) {
	this.materialGallery = materialGallery;
	return this;
}
public String getExplanationGallery() {
	return explanationGallery;
}
public Gallery setExplanationGallery(String explanationGallery) {
	this.explanationGallery = explanationGallery;
	return this;
}
public int getCategoryGalleryId() {
	return categoryGalleryId;
}
public Gallery setCategoryGalleryId(int categoryGalleryId) {
	this.categoryGalleryId = categoryGalleryId;
	return this;
}
public String getImgNameGallery() {
	return imgNameGallery;
}
public Gallery setImgNameGallery(String imgNameGallery) {
	this.imgNameGallery = imgNameGallery;
	return this;
}
}

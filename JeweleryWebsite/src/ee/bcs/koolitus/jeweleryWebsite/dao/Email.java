package ee.bcs.koolitus.jeweleryWebsite.dao;

public class Email {
	private int idEmail;
	private int idJewelery;
	private String senderEmail;
	private String message;

	public int getIdEmail() {
		return idEmail;
	}

	public Email setIdEmail(int idEmail) {
		this.idEmail = idEmail;
		return this;
	}

	public int getIdJewelery() {
		return idJewelery;

	}

	public Email setIdJewelery(int idJewelery) {
		this.idJewelery = idJewelery;
		return this;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public Email setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public Email setMessage(String message) {
		this.message = message;
		return this;
	}

	@Override
	public String toString() {
		return "Email[idJewelery=" + idJewelery + " , senderEmail=" + senderEmail + " message=" + message
				+ " , idEmail=" + idEmail + "]";
	}
}